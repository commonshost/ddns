#!/usr/bin/env node

const { promises: { Resolver } } = require('dns')
const fetch = require('node-fetch')
const pkg = require('../package.json')

const userAgent = `${pkg.name} v${pkg.version}`

const options = {
  origin: process.env.COMMONSHOST_DDNS_CORE_ORIGIN,
  hostname: process.env.COMMONSHOST_DDNS_HOSTNAME,
  audience: process.env.COMMONSHOST_DDNS_AUTH0_AUDIENCE,
  clientId: process.env.COMMONSHOST_DDNS_AUTH0_CLIENT_ID,
  clientSecret: process.env.COMMONSHOST_DDNS_AUTH0_CLIENT_SECRET,
  issuer: process.env.COMMONSHOST_DDNS_AUTH0_ISSUER
}

async function getToken (options) {
  const response = await fetch(`${options.issuer}oauth/token`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
      'user-agent': userAgent
    },
    body: JSON.stringify({
      grant_type: 'client_credentials',
      client_id: options.clientId,
      client_secret: options.clientSecret,
      audience: options.audience
    })
  })
  if (!response.ok) {
    throw new Error(response.status)
  }
  const data = await response.json()
  return data.access_token
}

async function main () {
  try {
    const resolver = new Resolver()
    const opendns = ['208.67.222.222', '208.67.220.220']
    resolver.setServers(opendns)
    const [ipv4Public] = await resolver.resolve4('myip.opendns.com')
    const serverId = encodeURIComponent(options.hostname)
    const accessToken = await getToken(options)
    const response = await fetch(`${options.origin}/v2/servers/${serverId}/ddns`, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        'authorization': `Bearer ${accessToken}`,
        'user-agent': userAgent
      },
      body: JSON.stringify({ ipv4: ipv4Public })
    })
    if (!response.ok) {
      throw new Error(`Unsuccessful response: ${response.status}`)
    }
    const { ipv4 } = await response.json()
    if (!ipv4) {
      throw new Error('Unexpected response')
    }
  } catch (error) {
    console.error(error.message)
    process.exit(1)
  }
}

main()
