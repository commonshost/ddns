const test = require('blue-tape')
const { createServer } = require('http')
const { once } = require('events')
const { spawn } = require('child_process')
const { join } = require('path')
const { isIPv4 } = require('net')

test('Start server', async (t) => {
  const auth0Server = createServer((request, response) => {
    console.log('AUTH0', request.method, request.url, request.headers)
    t.is(request.method, 'POST')
    t.is(request.url, '/oauth/token')
    response.end(JSON.stringify({
      access_token: 'test-access-token'
    }))
  })
  const coreServer = createServer(async (request, response) => {
    console.log('CORE', request.method, request.url, request.headers)
    const chunks = []
    for await (const chunk of request) {
      chunks.push(chunk)
    }
    const raw = Buffer.concat(chunks).toString('utf8')
    console.log('BODY', raw)
    t.is(request.method, 'PUT')
    t.is(request.url, `/v2/servers/aa-zzz-1/ddns`)
    t.ok(isIPv4(JSON.parse(raw).ipv4))
    response.end(JSON.stringify({ ipv4: request.socket.remoteAddress }))
  })

  auth0Server.listen(0)
  await once(auth0Server, 'listening')

  coreServer.listen(0)
  await once(coreServer, 'listening')

  const bin = join(__dirname, '../source/ddns.js')
  const child = spawn(process.execPath, [bin], {
    stdio: 'inherit',
    env: {
      ...process.env,
      COMMONSHOST_DDNS_CORE_ORIGIN:
        `http://localhost:${coreServer.address().port}`,
      COMMONSHOST_DDNS_HOSTNAME:
        'aa-zzz-1',
      COMMONSHOST_DDNS_AUTH0_AUDIENCE:
        'https://example.com/',
      COMMONSHOST_DDNS_AUTH0_CLIENT_ID:
        'test-client-id',
      COMMONSHOST_DDNS_AUTH0_CLIENT_SECRET:
        'test-client-secret',
      COMMONSHOST_DDNS_AUTH0_ISSUER:
        `http://localhost:${auth0Server.address().port}/`
    }
  })

  const [code] = await once(child, 'exit')
  t.is(code, 0)

  auth0Server.close()
  await once(auth0Server, 'close')

  coreServer.close()
  await once(coreServer, 'close')
})
